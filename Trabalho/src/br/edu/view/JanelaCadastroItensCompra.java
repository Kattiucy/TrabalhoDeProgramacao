package br.edu.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import br.edu.repository.BancoDados;

public class JanelaCadastroItensCompra extends JFrame {

	private static final long serialVersionUID = 6832829989494438253L;
	private JButton inserirButton;
	private JButton apagarButton;
	private JButton listarButton;
	private JButton voltarButton;
	
	private JLabel selecaoCompraLabel;
	private JComboBox<String> compraCombo;
	
	private JLabel selecaoProdutoLabel;
	private JComboBox<String> produtoCombo;
	
	private JPanel cadastroPanel;
	private JPanel botoesPanel;
	
	private Container container;
	
	private BancoDados db = new BancoDados();
		
	public JanelaCadastroItensCompra() {
		super("Cadastro de Itens de Compra");
		
		selecaoCompraLabel = new JLabel("Selecione a Compra na Lista");
		compraCombo = new JComboBox<String>();
		
		for ( String s : db.listarTodasCompras() ) {
			compraCombo.addItem(s);
		}
		
		selecaoProdutoLabel = new JLabel("Selecione o Produto na Lista");
		produtoCombo = new JComboBox<String>();
		
		for ( String s : db.listarTodosProdutos() ) {
			produtoCombo.addItem(s);
		}
					
		cadastroPanel = new JPanel(new GridLayout(8,1));
		cadastroPanel.setPreferredSize(new Dimension(400,150));
		cadastroPanel.add(selecaoCompraLabel);
		cadastroPanel.add(compraCombo);
		cadastroPanel.add(selecaoProdutoLabel);
		cadastroPanel.add(produtoCombo);
		
		inserirButton = new JButton("Inserir");
		inserirButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					compraCombo.getSelectedItem();
					String[] str = compraCombo.getSelectedItem().toString().split("-");
					Integer cod = Integer.valueOf(str[0].substring(7).trim());
					
					produtoCombo.getSelectedItem();
					String[] str2 = produtoCombo.getSelectedItem().toString().split("-");
					Integer cod2 = Integer.valueOf(str2[0].substring(7).trim());
					
					db.inserirItemCompra(cod2.intValue(), cod.intValue());
					JOptionPane.showMessageDialog(null,"Item de compra inserido com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
			
		});
		
		apagarButton = new JButton("Apagar");
		apagarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					String[] str = compraCombo.getSelectedItem().toString().split("-");
					Integer cod = Integer.valueOf(str[0].substring(7).trim());
					
					produtoCombo.getSelectedItem();
					String[] str2 = produtoCombo.getSelectedItem().toString().split("-");
					Integer cod2 = Integer.valueOf(str2[0].substring(7).trim());
					
					db.apagarItemCompra(cod.intValue(), cod2.intValue());

					JOptionPane.showMessageDialog(null,"Produto removido da compra com sucesso!!");
				} catch (SQLException e) {
					JOptionPane.showMessageDialog(null, "Erro, "+e.getMessage());
				}
			}
			
		});	
			
		
		listarButton = new JButton("Listagem");
		listarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				new JanelaListagemItensCompras();
			}
			
		});
		
		voltarButton = new JButton("Voltar");
		voltarButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
			
		});		
		
		botoesPanel = new JPanel(new GridLayout(1,6));
		botoesPanel.setPreferredSize(new Dimension(100,50));
		botoesPanel.add(inserirButton);
		botoesPanel.add(apagarButton);
		botoesPanel.add(listarButton);
		botoesPanel.add(voltarButton);
		
		container = getContentPane();
		container.setLayout(new BorderLayout());
		container.add(cadastroPanel, BorderLayout.CENTER);
		container.add(botoesPanel, BorderLayout.SOUTH);
		
		setSize(900,300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setVisible(false);
		
	}
	
	public void carregarListaCompra() {
		compraCombo.removeAllItems();
				
		for ( String s : db.listarTodasCompras() ) {
			compraCombo.addItem(s);
		}
		
	}
	
	public void carregarListaProdutos() {
		produtoCombo.removeAllItems();
				
		for ( String s : db.listarTodosProdutos() ) {
			produtoCombo.addItem(s);
		}
		
	}
	
}
